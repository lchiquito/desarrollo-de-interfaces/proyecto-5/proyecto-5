<?php

namespace app\controllers;

use app\models\Peces;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PecesController implements the CRUD actions for Peces model.
 */
class PecesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Peces models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Peces::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Peces model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Peces model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Peces();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Peces model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Peces model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Peces model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Peces the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Peces::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }   
    
       /** consulta para que muestre los peces que tengo en la base de datos junto con el nombre, el precio, la sombra y el lugar de aparicion*/
      public function actionConsultaPeces()
    {
    $dataProvider = new ActiveDataProvider([
        'query' => Peces::find()->select('id, nombre, precio, sombra, lugaraparicion'),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render('consulta-peces', [
        'dataProvider' => $dataProvider,
    ]);
}

/* CONSULTAS PARA EL DESPLEGABLE DE LAS SOMBRAS */

/* Consulta para que muestre la sombra pequeña de los peces en el desplegable */
    public function actionMuypequena(){
     $dataProvider = new ActiveDataProvider([
        'query' => Peces::find()
            ->select(['id','nombre'])
            ->where(['sombra' => 'muy pequeña']),
             ]);
        return $this->render("peces-con-sombra-muy-pequena",[
            "dataProvider"=>$dataProvider,
        ]);
    }

/* Consulta para que muestre la sombra pequeña de los peces en el desplegable */
    public function actionPequena(){
     $dataProvider = new ActiveDataProvider([
        'query' => Peces::find()
            ->select(['id','nombre'])
            ->where(['sombra' => 'pequeña']),
         'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("peces-con-sombra-pequena",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestre la sombra mediana de los peces en el desplegable */
    public function actionMediana(){
     $dataProvider = new ActiveDataProvider([
        'query' => Peces::find()
            ->select(['id','nombre'])
            ->where(['sombra' => 'mediana']),
         'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("peces-con-sombra-mediana",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestre la sombra grande de los peces en el desplegable */      
   public function actionGrande(){
     $dataProvider = new ActiveDataProvider([
        'query' => Peces::find()
            ->select(['id','nombre'])
            ->where(['sombra' => 'grande']),
         'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("peces-con-sombra-grande",[
            "dataProvider"=>$dataProvider,
        ]);
    } 
    
/* Consulta para que muestre la sombra estrecha de los peces en el desplegable */  
    public function actionEstrecha(){
     $dataProvider = new ActiveDataProvider([
        'query' => Peces::find()
            ->select(['id','nombre'])
            ->where(['sombra' => 'estrecha']),
         'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("peces-con-sombra-estrecha",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestre la sombra gigante de los peces en el desplegable */    
    public function actionGigante(){
     $dataProvider = new ActiveDataProvider([
        'query' => Peces::find()
            ->select(['id','nombre'])
            ->where(['sombra' => 'gigante']),
         'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("peces-con-sombra-gigante",[
            "dataProvider"=>$dataProvider,
        ]);
    }
  
 /* CONSULTAS PARA EL DESPLEGABLE DE LAS LUGARES DE APARICIÓN */
  
/* Consulta para que muestre el lugar de aparición en el rio de los peces en el desplegable */  
    public function actionRio(){
     $dataProvider = new ActiveDataProvider([
        'query' => Peces::find()
            ->select(['id','nombre'])
            ->where(['lugaraparicion' => 'rio']),
         'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("lugar-aparicion-rio",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestre el lugar de aparición en el mar de los peces en el desplegable */  
    public function actionMar(){
     $dataProvider = new ActiveDataProvider([
        'query' => Peces::find()
            ->select(['id','nombre'])
            ->where(['lugaraparicion' => 'mar']),
         'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("lugar-aparicion-mar",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestre el lugar de aparición en el muelle de los peces en el desplegable */  
    public function actionMuelle(){
     $dataProvider = new ActiveDataProvider([
        'query' => Peces::find()
            ->select(['id','nombre'])
            ->where(['lugaraparicion' => 'muelle']),
         'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("lugar-aparicion-muelle",[
            "dataProvider"=>$dataProvider,
        ]);
    }   
    
/* Consulta para que muestre el lugar de aparición en el estanque de los peces en el desplegable */  
    public function actionEstanque(){
     $dataProvider = new ActiveDataProvider([
        'query' => Peces::find()
            ->select(['id','nombre'])
            ->where(['lugaraparicion' => 'estanque']),
         'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("lugar-aparicion-estanque",[
            "dataProvider"=>$dataProvider,
        ]);
    } 
    
/* CONSULTAS PARA EL DESPLEGABLE DE LOS MESES*/ 
    /* Consulta para que muestre los peces de enero */  
    public function actionPecesenero() {
    $dataProvider = new ActiveDataProvider([
        'query' => Peces::find()
            ->select(['peces.id','nombre'])
            ->innerJoin('mesespeces', 'peces.id = mesespeces.idpeces')
            ->where(['mesespeces.meses' => 'enero']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("peces-enero", [
        "dataProvider" => $dataProvider,
    ]);
}

/* Consulta para que muestre los peces de febrero */  
    public function actionPecesfebrero() {
    $dataProvider = new ActiveDataProvider([
        'query' => Peces::find()
            ->select(['peces.id','nombre'])
            ->innerJoin('mesespeces', 'peces.id = mesespeces.idpeces')
            ->where(['mesespeces.meses' => 'febrero']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("peces-febrero", [
        "dataProvider" => $dataProvider,
    ]);
}

   /* Consulta para que muestre los peces de marzo */  
    public function actionPecesmarzo() {
    $dataProvider = new ActiveDataProvider([
        'query' => Peces::find()
            ->select(['peces.id','nombre'])
            ->innerJoin('mesespeces', 'peces.id = mesespeces.idpeces')
            ->where(['mesespeces.meses' => 'marzo']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("peces-marzo", [
        "dataProvider" => $dataProvider,
    ]);
}

   /* Consulta para que muestre los peces de abril */  
    public function actionPecesabril() {
    $dataProvider = new ActiveDataProvider([
        'query' => Peces::find()
            ->select(['peces.id','nombre'])
            ->innerJoin('mesespeces', 'peces.id = mesespeces.idpeces')
            ->where(['mesespeces.meses' => 'abril']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("peces-abril", [
        "dataProvider" => $dataProvider,
    ]);
}

   /* Consulta para que muestre los peces de mayo */  
    public function actionPecesmayo() {
    $dataProvider = new ActiveDataProvider([
        'query' => Peces::find()
            ->select(['peces.id','nombre'])
            ->innerJoin('mesespeces', 'peces.id = mesespeces.idpeces')
            ->where(['mesespeces.meses' => 'mayo']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("peces-mayo", [
        "dataProvider" => $dataProvider,
    ]);
}
    
   /* Consulta para que muestre los peces de junio */  
    public function actionPecesjunio() {
    $dataProvider = new ActiveDataProvider([
        'query' => Peces::find()
            ->select(['peces.id','nombre'])
            ->innerJoin('mesespeces', 'peces.id = mesespeces.idpeces')
            ->where(['mesespeces.meses' => 'junio']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("peces-junio", [
        "dataProvider" => $dataProvider,
    ]);
}

   /* Consulta para que muestre los peces de julio */  
    public function actionPecesjulio() {
    $dataProvider = new ActiveDataProvider([
        'query' => Peces::find()
            ->select(['peces.id','nombre'])
            ->innerJoin('mesespeces', 'peces.id = mesespeces.idpeces')
            ->where(['mesespeces.meses' => 'julio']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("peces-julio", [
        "dataProvider" => $dataProvider,
    ]);
}

   /* Consulta para que muestre los peces de agosto */  
    public function actionPecesagosto() {
    $dataProvider = new ActiveDataProvider([
        'query' => Peces::find()
            ->select(['peces.id','nombre'])
            ->innerJoin('mesespeces', 'peces.id = mesespeces.idpeces')
            ->where(['mesespeces.meses' => 'agosto']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("peces-agosto", [
        "dataProvider" => $dataProvider,
    ]);
}
    
   /* Consulta para que muestre los peces de septiembre */  
    public function actionPecesseptiembre() {
    $dataProvider = new ActiveDataProvider([
        'query' => Peces::find()
            ->select(['peces.id','nombre'])
            ->innerJoin('mesespeces', 'peces.id = mesespeces.idpeces')
            ->where(['mesespeces.meses' => 'septiembre']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("peces-septiembre", [
        "dataProvider" => $dataProvider,
    ]);
}

   /* Consulta para que muestre los peces de octubre */  
    public function actionPecesoctubre() {
    $dataProvider = new ActiveDataProvider([
        'query' => Peces::find()
            ->select(['peces.id','nombre'])
            ->innerJoin('mesespeces', 'peces.id = mesespeces.idpeces')
            ->where(['mesespeces.meses' => 'octubre']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("peces-octubre", [
        "dataProvider" => $dataProvider,
    ]);
}
    
   /* Consulta para que muestre los peces de noviembre */  
    public function actionPecesnoviembre() {
    $dataProvider = new ActiveDataProvider([
        'query' => Peces::find()
            ->select(['peces.id','nombre'])
            ->innerJoin('mesespeces', 'peces.id = mesespeces.idpeces')
            ->where(['mesespeces.meses' => 'noviembre']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("peces-noviembre", [
        "dataProvider" => $dataProvider,
    ]);
}

   /* Consulta para que muestre los peces de diciembre */  
    public function actionPecesdiciembre() {
    $dataProvider = new ActiveDataProvider([
        'query' => Peces::find()
            ->select(['peces.id','nombre'])
            ->innerJoin('mesespeces', 'peces.id = mesespeces.idpeces')
            ->where(['mesespeces.meses' => 'diciembre']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("peces-diciembre", [
        "dataProvider" => $dataProvider,
    ]);
}
    
    
    /*


 public function actionPecesConSombraPequena()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Peces::find()
                ->select(['nombre', 'sombra'])
                ->where(['sombra' => 'pequeña']),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $this->render('peces-con-sombra-pequena', [
            'dataProvider' => $dataProvider,
        ]);
    }
    */
}

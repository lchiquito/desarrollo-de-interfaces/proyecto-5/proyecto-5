<?php

namespace app\controllers;

use app\models\Obrasdearte;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ObrasdearteController implements the CRUD actions for Obrasdearte model.
 */
class ObrasdearteController extends Controller
{
    /**
     * @inheritDoc
     */
    
    /** consulta para que muestre las obras de darte que tengo en la base de datos */
    public function actionMostrarobras (){
        $datosObras = new ActiveDataProvider([
            'query' => Obrasdearte::find(),
        ]);
        
        return $this->render("obrasdearte",[
            
            "dataProvider"=>$dataProvider,
        ]);
    }
    
    /** consulta para que muestre las obras de arte que tengo en la base de datos junto con el título y el autor */
      public function actionConsultaObrasArte()
    {
    $dataProvider = new ActiveDataProvider([
        'query' => ObrasDeArte::find()->select('id, titulo, autor, falsificacion, explicacion'),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render('consulta-obras-arte', [
        'dataProvider' => $dataProvider,
    ]);
}
    
    
    
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Obrasdearte models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Obrasdearte::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Obrasdearte model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Obrasdearte model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Obrasdearte();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Obrasdearte model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Obrasdearte model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Obrasdearte model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Obrasdearte the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Obrasdearte::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    
/* CONSULTAS PARA DESPLEGABLE DE FALSIFICACIONES*/    
 /* Consulta para que muestren las obras con falsificación*/
    public function actionConfalsificacion(){
     $dataProvider = new ActiveDataProvider([
        'query' => Obrasdearte::find()
            ->select(['id', 'titulo'])
            ->where(['falsificacion' => 'si']),
          'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("obras-con-falsificacion",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
 /* Consulta para que muestren las obras sin falsificación*/
    public function actionSinfalsificacion(){
     $dataProvider = new ActiveDataProvider([
        'query' => Obrasdearte::find()
            ->select(['id','titulo'])
            ->where(['falsificacion' => 'no']),
          'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("obras-sin-falsificacion",[
            "dataProvider"=>$dataProvider,
        ]);
    }
}

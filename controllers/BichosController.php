<?php

namespace app\controllers;

use app\models\Bichos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BichosController implements the CRUD actions for Bichos model.
 */
class BichosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Bichos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Bichos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Bichos model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Bichos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Bichos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Bichos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Bichos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Bichos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Bichos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Bichos::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
       /** consulta para que muestre los bichos que tengo en la base de datos junto con el nombre, el precio, la tiempo y el lugar de aparición */
      public function actionConsultaBichos()
    {
    $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()->select('id, nombre, precio, tiempo, lugaraparicion'),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render('consulta-bichos', [
        'dataProvider' => $dataProvider,
    ]);
}

/* CONSULTAS PARA EL DESPLEGABLE DEL TIEMPO */

/* Consulta para que muestre los bichos que salen siempre */
    public function actionSiempre(){
     $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['id','nombre'])
            ->where(['tiempo' => 'siempre']),
          'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("bichos-salen-siempre",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestre los bichos que salen siempre excepto con lluvia */
    public function actionSiempresinlluvia(){
     $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['id','nombre'])
            ->where(['tiempo' => 'siempre menos con lluvia']),
          'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("bichos-salen-siempre-sin-lluvia",[
            "dataProvider"=>$dataProvider,
        ]);
    } 
    
/* Consulta para que muestre los bichos que salen solo con lluvia */
    public function actionSoloconlluvia(){
     $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['id','nombre'])
            ->where(['tiempo' => 'solo con lluvia']),
          'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("bichos-salen-solo-con-lluvia",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* CONSULTAS PARA EL LUGAR DE APARICIÓN */
    /* Consulta para que muestre los bichos que salen cerca de las flores*/
    public function actionCercadeflores(){
     $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['id','nombre'])
            ->where(['lugaraparicion' => 'cerca de flores']),
          'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("bichos-cerca-de-flores",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestre los bichos que salen agitando árboles*/
    public function actionAgitandoarboles(){
     $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['id','nombre'])
            ->where(['lugaraparicion' => 'agitando arboles']),
          'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("bichos-agitando-arboles",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestre los bichos que salen en la orilla del mar*/
    public function actionOrillademar(){
     $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['id','nombre'])
            ->where(['lugaraparicion' => 'en la orilla del mar']),
          'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("bichos-orilla-de-mar",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestre los bichos que salen en rocas o arbustos */
    public function actionRocasarbustos(){
     $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['id','nombre'])
            ->where(['lugaraparicion' => 'en rocas o arbustos']),
          'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("bichos-rocas-arbustos",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestre los bichos que salen en las flores */
    public function actionFlores(){
     $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['id','nombre'])
            ->where(['lugaraparicion' => 'en las flores']),
          'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("bichos-en-flores",[
            "dataProvider"=>$dataProvider,
        ]);
    }
        
/* Consulta para que muestre los bichos que salen en charcas y ríos */
    public function actionCharcas(){
     $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['id','nombre'])
            ->where(['lugaraparicion' => 'en charcas y rios']),
          'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("bichos-rios-charcas",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestre los bichos que salen golpeando rocas */
    public function actionRocas(){
     $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['id','nombre'])
            ->where(['lugaraparicion' => 'dandole a rocas']),
          'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("bichos-rocas",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestre los bichos que salen en los árboles */
    public function actionEnarboles(){
     $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['id','nombre'])
            ->where(['lugaraparicion' => 'en los arboles']),
          'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("bichos-en-arboles",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestre los bichos que salen en palmeras */
    public function actionPalmeras(){
     $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['id','nombre'])
            ->where(['lugaraparicion' => 'en palmeras']),
          'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("bichos-en-palmeras",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestre los bichos que salen en el suelo */
    public function actionSuelo(){
     $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['id','nombre'])
            ->where(['lugaraparicion' => 'en el suelo']),
          'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("bichos-en-suelo",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestre los bichos que salen en tocones de árbol*/
    public function actionTocones(){
     $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['id','nombre'])
            ->where(['lugaraparicion' => 'en tocones de arbol']),
          'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("bichos-en-tocones",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestre los bichos que salen volando*/
    public function actionVolando(){
     $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['id','nombre'])
            ->where(['lugaraparicion' => 'volando']),
          'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("bichos-volando",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestre los bichos que salen en aldeanos*/
    public function actionAldeanos(){
     $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['id','nombre'])
            ->where(['lugaraparicion' => 'en los aldeanos']),
          'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("bichos-aldeanos",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestre los bichos que salen en la hierba*/
    public function actionHierba(){
     $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['id','nombre'])
            ->where(['lugaraparicion' => 'en la hierba']),
          'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
             ]);
        return $this->render("bichos-hierba",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* CONSULTAS PARA EL DESPLEGABLE DE LOS MESES*/ 
    /* Consulta para que muestre los bichos de enero */  
    public function actionBichosenero() {
    $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['bichos.id','nombre'])
            ->innerJoin('mesesbichos', 'bichos.id = mesesbichos.idbichos')
            ->where(['mesesbichos.meses' => 'enero']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("bichos-enero", [
        "dataProvider" => $dataProvider,
    ]);
}

    /* Consulta para que muestre los bichos de febrero */  
    public function actionBichosfebrero() {
    $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['bichos.id','nombre'])
            ->innerJoin('mesesbichos', 'bichos.id = mesesbichos.idbichos')
            ->where(['mesesbichos.meses' => 'febrero']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("bichos-febrero", [
        "dataProvider" => $dataProvider,
    ]);
}

    /* Consulta para que muestre los bichos de marzo */  
    public function actionBichosmarzo() {
    $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['bichos.id','nombre'])
            ->innerJoin('mesesbichos', 'bichos.id = mesesbichos.idbichos')
            ->where(['mesesbichos.meses' => 'marzo']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("bichos-marzo", [
        "dataProvider" => $dataProvider,
    ]);
}

    /* Consulta para que muestre los bichos de abril */  
    public function actionBichosabril() {
    $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['bichos.id','nombre'])
            ->innerJoin('mesesbichos', 'bichos.id = mesesbichos.idbichos')
            ->where(['mesesbichos.meses' => 'abril']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("bichos-abril", [
        "dataProvider" => $dataProvider,
    ]);
}

    /* Consulta para que muestre los bichos de mayo */  
    public function actionBichosmayo() {
    $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['bichos.id','nombre'])
            ->innerJoin('mesesbichos', 'bichos.id = mesesbichos.idbichos')
            ->where(['mesesbichos.meses' => 'mayo']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("bichos-mayo", [
        "dataProvider" => $dataProvider,
    ]);
}

    /* Consulta para que muestre los bichos de junio */  
    public function actionBichosjunio() {
    $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['bichos.id','nombre'])
            ->innerJoin('mesesbichos', 'bichos.id = mesesbichos.idbichos')
            ->where(['mesesbichos.meses' => 'junio']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("bichos-junio", [
        "dataProvider" => $dataProvider,
    ]);
}

    /* Consulta para que muestre los bichos de julio */  
    public function actionBichosjulio() {
    $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['bichos.id','nombre'])
            ->innerJoin('mesesbichos', 'bichos.id = mesesbichos.idbichos')
            ->where(['mesesbichos.meses' => 'julio']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("bichos-julio", [
        "dataProvider" => $dataProvider,
    ]);
}

    /* Consulta para que muestre los bichos de agosto */  
    public function actionBichosagosto() {
    $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['bichos.id','nombre'])
            ->innerJoin('mesesbichos', 'bichos.id = mesesbichos.idbichos')
            ->where(['mesesbichos.meses' => 'agosto']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("bichos-agosto", [
        "dataProvider" => $dataProvider,
    ]);
}

    /* Consulta para que muestre los bichos de septiembre */  
    public function actionBichosseptiembre() {
    $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['bichos.id','nombre'])
            ->innerJoin('mesesbichos', 'bichos.id = mesesbichos.idbichos')
            ->where(['mesesbichos.meses' => 'septiembre']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("bichos-septiembre", [
        "dataProvider" => $dataProvider,
    ]);
}

    /* Consulta para que muestre los bichos de octubre */  
    public function actionBichosoctubre() {
    $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['bichos.id','nombre'])
            ->innerJoin('mesesbichos', 'bichos.id = mesesbichos.idbichos')
            ->where(['mesesbichos.meses' => 'octubre']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("bichos-octubre", [
        "dataProvider" => $dataProvider,
    ]);
}

    /* Consulta para que muestre los bichos de noviembre */  
    public function actionBichosnoviembre() {
    $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['bichos.id','nombre'])
            ->innerJoin('mesesbichos', 'bichos.id = mesesbichos.idbichos')
            ->where(['mesesbichos.meses' => 'noviembre']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("bichos-noviembre", [
        "dataProvider" => $dataProvider,
    ]);
}

    /* Consulta para que muestre los bichos de diciembre */  
    public function actionBichosdiciembre() {
    $dataProvider = new ActiveDataProvider([
        'query' => Bichos::find()
            ->select(['bichos.id','nombre'])
            ->innerJoin('mesesbichos', 'bichos.id = mesesbichos.idbichos')
            ->where(['mesesbichos.meses' => 'diciembre']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("bichos-diciembre", [
        "dataProvider" => $dataProvider,
    ]);
}

}

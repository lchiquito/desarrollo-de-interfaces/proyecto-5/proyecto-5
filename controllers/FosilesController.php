<?php

namespace app\controllers;

use app\models\Fosiles;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FosilesController implements the CRUD actions for Fosiles model.
 */
class FosilesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Fosiles models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Fosiles::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Fosiles model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Fosiles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Fosiles();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Fosiles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Fosiles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Fosiles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Fosiles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Fosiles::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
       
    public function actionConsultaFosiles()
    {
    $dataProvider = new ActiveDataProvider([
        'query' => Fosiles::find()->select('id, nombre, partes, precio'),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render('consulta-fosiles', [
        'dataProvider' => $dataProvider,
    ]);
}

/* CONSULTAS PARA EL DESPLEGABLE DE LOS FOSILES */

/* Consulta para que muestren los fósiles con 1 parte en el desplegable */
    public function action1(){
     $dataProvider = new ActiveDataProvider([
        'query' => Fosiles::find()
            ->select(['id','nombre'])
            ->where(['partes' => '1']),
             ]);
        return $this->render("fosiles-con-una-parte",[
            "dataProvider"=>$dataProvider,
        ]);
    }

/* Consulta para que muestren los fósiles con 2 partes en el desplegable */
    public function action2(){
     $dataProvider = new ActiveDataProvider([
        'query' => Fosiles::find()
            ->select(['id','nombre'])
            ->where(['partes' => '2']),
             ]);
        return $this->render("fosiles-con-dos-partes",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
    
/* Consulta para que muestren los fósiles con 3 partes en el desplegable */
    public function action3(){
     $dataProvider = new ActiveDataProvider([
        'query' => Fosiles::find()
            ->select(['id','nombre'])
            ->where(['partes' => '3']),
             ]);
        return $this->render("fosiles-con-tres-partes",[
            "dataProvider"=>$dataProvider,
        ]);
    }   
    
/* Consulta para que muestren los fósiles con 4 partes en el desplegable */
    public function action4(){
     $dataProvider = new ActiveDataProvider([
        'query' => Fosiles::find()
            ->select(['id','nombre'])
            ->where(['partes' => '4']),
             ]);
        return $this->render("fosiles-con-cuarto-partes",[
            "dataProvider"=>$dataProvider,
        ]);
    }     
}

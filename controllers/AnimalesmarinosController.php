<?php

namespace app\controllers;

use app\models\Animalesmarinos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AnimalesmarinosController implements the CRUD actions for Animalesmarinos model.
 */
class AnimalesmarinosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Animalesmarinos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Animalesmarinos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Animalesmarinos model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Animalesmarinos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Animalesmarinos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Animalesmarinos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Animalesmarinos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Animalesmarinos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Animalesmarinos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Animalesmarinos::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    /** consulta para que muestre los animales marinos que tengo en la base de datos junto con el nombre, el precio, la sombra y la velocidad */
      public function actionConsultaAnimalesmarinos()
    {
    $dataProvider = new ActiveDataProvider([
        'query' => Animalesmarinos::find()->select('id, nombre, precio, sombra, velocidad'),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render('consulta-animales-marinos', [
        'dataProvider' => $dataProvider,
    ]);
}

/* CONSULTAS PARA EL DESPLEGABLE DE LAS SOMBRAS */

/* Consulta para que muestre la sombra muy pequeña de los animales marinos en el desplegable */
    public function actionMuypequena(){
     $dataProvider = new ActiveDataProvider([
        'query' => Animalesmarinos::find()
            ->select(['id','nombre'])
            ->where(['sombra' => 'muy pequeña']),
             ]);
        return $this->render("animales-marinos-sombra-muy-pequena",[
            "dataProvider"=>$dataProvider,
        ]);
    }

/* Consulta para que muestre la sombra pequeña de los animales marinos en el desplegable */
    public function actionPequena(){
     $dataProvider = new ActiveDataProvider([
        'query' => Animalesmarinos::find()
            ->select(['id','nombre'])
            ->where(['sombra' => 'pequeña']),
             ]);
        return $this->render("animales-marinos-sombra-pequena",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestre la sombra mediana de los animales marinos en el desplegable */
    public function actionMediana(){
     $dataProvider = new ActiveDataProvider([
        'query' => Animalesmarinos::find()
            ->select(['id','nombre'])
            ->where(['sombra' => 'mediana']),
             ]);
        return $this->render("animales-marinos-sombra-mediana",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestre la sombra grande de los animales marinos en el desplegable */
    public function actionGrande(){
     $dataProvider = new ActiveDataProvider([
        'query' => Animalesmarinos::find()
            ->select(['id','nombre'])
            ->where(['sombra' => 'grande']),
             ]);
        return $this->render("animales-marinos-sombra-grande",[
            "dataProvider"=>$dataProvider,
        ]);
    }

/* CONSULTAS PARA EL DESPLEGABLE DE LA VELOCIDAD */  
    
/* Consulta para que muestren los animales marinos sin velocidad en el desplegable */
      public function actionSinvelocidad(){
     $dataProvider = new ActiveDataProvider([
        'query' => Animalesmarinos::find()
            ->select(['id','nombre'])
            ->where(['velocidad' => 'no tiene']),
             ]);
        return $this->render("animales-marinos-sin-velocidad",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestren los animales marinos muy lentos en el desplegable */
      public function actionMuylenta(){
     $dataProvider = new ActiveDataProvider([
        'query' => Animalesmarinos::find()
            ->select(['id','nombre'])
            ->where(['velocidad' => 'muy lenta']),
             ]);
        return $this->render("animales-marinos-muy-lentos",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestren los animales marinos lentos en el desplegable */
      public function actionLenta(){
     $dataProvider = new ActiveDataProvider([
        'query' => Animalesmarinos::find()
            ->select(['id','nombre'])
            ->where(['velocidad' => 'lenta']),
             ]);
        return $this->render("animales-marinos-lentos",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestren los animales marinos con velocidad normal en el desplegable */
      public function actionNormal(){
     $dataProvider = new ActiveDataProvider([
        'query' => Animalesmarinos::find()
            ->select(['id','nombre'])
            ->where(['velocidad' => 'mediana']),
             ]);
        return $this->render("animales-marinos-velocidad-normal",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestren los animales marinos rápidos en el desplegable */
      public function actionRapida(){
     $dataProvider = new ActiveDataProvider([
        'query' => Animalesmarinos::find()
            ->select(['id','nombre'])
            ->where(['velocidad' => 'rapida']),
             ]);
        return $this->render("animales-marinos-rapidos",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* Consulta para que muestren los animales marinos sin velocidad en el desplegable */
      public function actionMuyrapida(){
     $dataProvider = new ActiveDataProvider([
        'query' => Animalesmarinos::find()
            ->select(['id','nombre'])
            ->where(['velocidad' => 'muy rapida']),
             ]);
        return $this->render("animales-marinos-muy-rapidos",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
/* CONSULTAS PARA EL DESPLEGABLE DE LOS MESES*/ 
    /* Consulta para que muestre los animales marinos de enero */  
    public function actionAmenero() {
    $dataProvider = new ActiveDataProvider([
        'query' => Animalesmarinos::find()
            ->select(['animalesmarinos.id','nombre'])
            ->innerJoin('mesesam', 'animalesmarinos.id = mesesam.idam')
            ->where(['mesesam.meses' => 'enero']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("am-enero", [
        "dataProvider" => $dataProvider,
    ]);
}

  /* Consulta para que muestre los animales marinos de febrero */  
    public function actionAmfebrero() {
    $dataProvider = new ActiveDataProvider([
        'query' => Animalesmarinos::find()
            ->select(['animalesmarinos.id','nombre'])
            ->innerJoin('mesesam', 'animalesmarinos.id = mesesam.idam')
            ->where(['mesesam.meses' => 'febrero']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("am-febrero", [
        "dataProvider" => $dataProvider,
    ]);
}

  /* Consulta para que muestre los animales marinos de marzo */  
    public function actionAmmarzo() {
    $dataProvider = new ActiveDataProvider([
        'query' => Animalesmarinos::find()
            ->select(['animalesmarinos.id','nombre'])
            ->innerJoin('mesesam', 'animalesmarinos.id = mesesam.idam')
            ->where(['mesesam.meses' => 'marzo']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("am-marzo", [
        "dataProvider" => $dataProvider,
    ]);
}

  /* Consulta para que muestre los animales marinos de abril */  
    public function actionAmabril() {
    $dataProvider = new ActiveDataProvider([
        'query' => Animalesmarinos::find()
            ->select(['animalesmarinos.id','nombre'])
            ->innerJoin('mesesam', 'animalesmarinos.id = mesesam.idam')
            ->where(['mesesam.meses' => 'abril']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("am-abril", [
        "dataProvider" => $dataProvider,
    ]);
}

  /* Consulta para que muestre los animales marinos de mayo */  
    public function actionAmmayo() {
    $dataProvider = new ActiveDataProvider([
        'query' => Animalesmarinos::find()
            ->select(['animalesmarinos.id','nombre'])
            ->innerJoin('mesesam', 'animalesmarinos.id = mesesam.idam')
            ->where(['mesesam.meses' => 'mayo']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("am-mayo", [
        "dataProvider" => $dataProvider,
    ]);
}

  /* Consulta para que muestre los animales marinos de junio */  
    public function actionAmjunio() {
    $dataProvider = new ActiveDataProvider([
        'query' => Animalesmarinos::find()
            ->select(['animalesmarinos.id','nombre'])
            ->innerJoin('mesesam', 'animalesmarinos.id = mesesam.idam')
            ->where(['mesesam.meses' => 'junio']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("am-junio", [
        "dataProvider" => $dataProvider,
    ]);
}

  /* Consulta para que muestre los animales marinos de julio */  
    public function actionAmjulio() {
    $dataProvider = new ActiveDataProvider([
        'query' => Animalesmarinos::find()
            ->select(['animalesmarinos.id','nombre'])
            ->innerJoin('mesesam', 'animalesmarinos.id = mesesam.idam')
            ->where(['mesesam.meses' => 'julio']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("am-julio", [
        "dataProvider" => $dataProvider,
    ]);
}

  /* Consulta para que muestre los animales marinos de agosto */  
    public function actionAmagosto() {
    $dataProvider = new ActiveDataProvider([
        'query' => Animalesmarinos::find()
            ->select(['animalesmarinos.id','nombre'])
            ->innerJoin('mesesam', 'animalesmarinos.id = mesesam.idam')
            ->where(['mesesam.meses' => 'agosto']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("am-agosto", [
        "dataProvider" => $dataProvider,
    ]);
}

  /* Consulta para que muestre los animales marinos de septiembre */  
    public function actionAmseptiembre() {
    $dataProvider = new ActiveDataProvider([
        'query' => Animalesmarinos::find()
            ->select(['animalesmarinos.id','nombre'])
            ->innerJoin('mesesam', 'animalesmarinos.id = mesesam.idam')
            ->where(['mesesam.meses' => 'septiembre']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("am-septiembre", [
        "dataProvider" => $dataProvider,
    ]);
}

  /* Consulta para que muestre los animales marinos de octubre */  
    public function actionAmoctubre() {
    $dataProvider = new ActiveDataProvider([
        'query' => Animalesmarinos::find()
            ->select(['animalesmarinos.id','nombre'])
            ->innerJoin('mesesam', 'animalesmarinos.id = mesesam.idam')
            ->where(['mesesam.meses' => 'octubre']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("am-octubre", [
        "dataProvider" => $dataProvider,
    ]);
}

  /* Consulta para que muestre los animales marinos de noviembre */  
    public function actionAmnoviembre() {
    $dataProvider = new ActiveDataProvider([
        'query' => Animalesmarinos::find()
            ->select(['animalesmarinos.id','nombre'])
            ->innerJoin('mesesam', 'animalesmarinos.id = mesesam.idam')
            ->where(['mesesam.meses' => 'noviembre']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("am-noviembre", [
        "dataProvider" => $dataProvider,
    ]);
}

  /* Consulta para que muestre los animales marinos de diciembre */  
    public function actionAmdiciembre() {
    $dataProvider = new ActiveDataProvider([
        'query' => Animalesmarinos::find()
            ->select(['animalesmarinos.id','nombre'])
            ->innerJoin('mesesam', 'animalesmarinos.id = mesesam.idam')
            ->where(['mesesam.meses' => 'diciembre']),
        'pagination' => [
            'pageSize' => -1, // Establece el número de registros por página
        ],
    ]);

    return $this->render("am-diciembre", [
        "dataProvider" => $dataProvider,
    ]);
}

}

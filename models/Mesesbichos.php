<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mesesbichos".
 *
 * @property int $id
 * @property int|null $idbichos
 * @property string|null $meses
 *
 * @property Bichos $idbichos0
 */
class Mesesbichos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mesesbichos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idbichos'], 'integer'],
            [['meses'], 'string', 'max' => 10],
            [['idbichos', 'meses'], 'unique', 'targetAttribute' => ['idbichos', 'meses']],
            [['idbichos'], 'exist', 'skipOnError' => true, 'targetClass' => Bichos::class, 'targetAttribute' => ['idbichos' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idbichos' => 'Idbichos',
            'meses' => 'Meses',
        ];
    }

    /**
     * Gets query for [[Idbichos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdbichos0()
    {
        return $this->hasOne(Bichos::class, ['id' => 'idbichos']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "animalesmarinos".
 *
 * @property int $id
 * @property string|null $nombre
 * @property int|null $precio
 * @property string|null $sombra
 * @property string|null $velocidad
 *
 * @property Consiguen[] $consiguens
 * @property Jugadores[] $idjugadores
 * @property Mesesam[] $mesesams
 */
class Animalesmarinos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'animalesmarinos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['precio'], 'integer'],
            [['nombre'], 'string', 'max' => 40],
            [['sombra'], 'string', 'max' => 15],
            [['velocidad'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'precio' => 'Precio',
            'sombra' => 'Sombra',
            'velocidad' => 'Velocidad',
        ];
    }

    /**
     * Gets query for [[Consiguens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConsiguens()
    {
        return $this->hasMany(Consiguen::class, ['idam' => 'id']);
    }

    /**
     * Gets query for [[Idjugadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdjugadores()
    {
        return $this->hasMany(Jugadores::class, ['id' => 'idjugadores'])->viaTable('consiguen', ['idam' => 'id']);
    }

    /**
     * Gets query for [[Mesesams]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMesesams()
    {
        return $this->hasMany(Mesesam::class, ['idam' => 'id']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "desentierran".
 *
 * @property int $id
 * @property int|null $idjugadores
 * @property int|null $idfosil
 *
 * @property Fosiles $idfosil0
 * @property Jugadores $idjugadores0
 */
class Desentierran extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'desentierran';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idjugadores', 'idfosil'], 'integer', 'min' => '1'],
            [['idjugadores', 'idfosil'], 'unique', 'targetAttribute' => ['idjugadores', 'idfosil']],
            [['idjugadores'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::class, 'targetAttribute' => ['idjugadores' => 'id']],
            [['idfosil'], 'exist', 'skipOnError' => true, 'targetClass' => Fosiles::class, 'targetAttribute' => ['idfosil' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idjugadores' => 'Idjugadores',
            'idfosil' => 'Idfosil',
        ];
    }

    /**
     * Gets query for [[Idfosil0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdfosil0()
    {
        return $this->hasOne(Fosiles::class, ['id' => 'idfosil']);
    }

    /**
     * Gets query for [[Idjugadores0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdjugadores0()
    {
        return $this->hasOne(Jugadores::class, ['id' => 'idjugadores']);
    }
}

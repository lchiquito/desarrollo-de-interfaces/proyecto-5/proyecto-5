<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "peces".
 *
 * @property int $id
 * @property string|null $nombre
 * @property int|null $precio
 * @property string|null $sombra
 * @property string|null $lugaraparicion
 *
 * @property Jugadores[] $idjugadores
 * @property Mesespeces[] $mesespeces
 * @property Pescan[] $pescans
 */
class Peces extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'peces';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['precio'], 'integer'],
            [['nombre'], 'string', 'max' => 30],
            [['sombra', 'lugaraparicion'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'precio' => 'Precio',
            'sombra' => 'Sombra',
            'lugaraparicion' => 'Lugar de aparición',
        ];
    }

    /**
     * Gets query for [[Idjugadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdjugadores()
    {
        return $this->hasMany(Jugadores::class, ['id' => 'idjugadores'])->viaTable('pescan', ['idpeces' => 'id']);
    }

    /**
     * Gets query for [[Mesespeces]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMesespeces()
    {
        return $this->hasMany(Mesespeces::class, ['idpeces' => 'id']);
    }

    /**
     * Gets query for [[Pescans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPescans()
    {
        return $this->hasMany(Pescan::class, ['idpeces' => 'id']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fosiles".
 *
 * @property int $id
 * @property string|null $nombre
 * @property int|null $partes
 * @property int|null $precio
 *
 * @property Desentierran[] $desentierrans
 * @property Jugadores[] $idjugadores
 */
class Fosiles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fosiles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['partes', 'precio'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'partes' => 'Partes',
            'precio' => 'Precio',
        ];
    }

    /**
     * Gets query for [[Desentierrans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDesentierrans()
    {
        return $this->hasMany(Desentierran::class, ['idfosil' => 'id']);
    }

    /**
     * Gets query for [[Idjugadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdjugadores()
    {
        return $this->hasMany(Jugadores::class, ['id' => 'idjugadores'])->viaTable('desentierran', ['idfosil' => 'id']);
    }
}

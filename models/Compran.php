<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "compran".
 *
 * @property int $id
 * @property int|null $idjugadores
 * @property int|null $idobra
 *
 * @property Jugadores $idjugadores0
 * @property Obrasdearte $idobra0
 */
class Compran extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'compran';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idjugadores', 'idobra'], 'integer', 'min' => '1'],
            [['idjugadores', 'idobra'], 'unique', 'targetAttribute' => ['idjugadores', 'idobra']],
            [['idjugadores'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::class, 'targetAttribute' => ['idjugadores' => 'id']],
            [['idobra'], 'exist', 'skipOnError' => true, 'targetClass' => Obrasdearte::class, 'targetAttribute' => ['idobra' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idjugadores' => 'Idjugadores',
            'idobra' => 'Idobra',
        ];
    }

    /**
     * Gets query for [[Idjugadores0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdjugadores0()
    {
        return $this->hasOne(Jugadores::class, ['id' => 'idjugadores']);
    }

    /**
     * Gets query for [[Idobra0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdobra0()
    {
        return $this->hasOne(Obrasdearte::class, ['id' => 'idobra']);
    }
}

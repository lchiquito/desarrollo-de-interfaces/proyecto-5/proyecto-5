<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "obrasdearte".
 *
 * @property int $id
 * @property string|null $titulo
 * @property string|null $autor
 * @property string|null $falsificacion
 * @property string|null $explicacion
 *
 * @property Compran[] $comprans
 * @property Jugadores[] $idjugadores
 */
class Obrasdearte extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'obrasdearte';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo'], 'string', 'max' => 50],
            [['autor'], 'string', 'max' => 30],
            [['falsificacion'], 'string', 'max' => 5],
            [['explicacion'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Título',
            'autor' => 'Autor',
            'falsificacion' => 'Falsificación',
            'explicacion' => 'Explicación',
        ];
    }

    /**
     * Gets query for [[Comprans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComprans()
    {
        return $this->hasMany(Compran::class, ['idobra' => 'id']);
    }

    /**
     * Gets query for [[Idjugadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdjugadores()
    {
        return $this->hasMany(Jugadores::class, ['id' => 'idjugadores'])->viaTable('compran', ['idobra' => 'id']);
    }
}

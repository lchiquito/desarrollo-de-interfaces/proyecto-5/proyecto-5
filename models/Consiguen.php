<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "consiguen".
 *
 * @property int $id
 * @property int|null $idjugadores
 * @property int|null $idam
 *
 * @property Animalesmarinos $idam0
 * @property Jugadores $idjugadores0
 */
class Consiguen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consiguen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idjugadores', 'idam'], 'integer', 'min' => '1'],
            [['idjugadores', 'idam'], 'unique', 'targetAttribute' => ['idjugadores', 'idam']],
            [['idam'], 'exist', 'skipOnError' => true, 'targetClass' => Animalesmarinos::class, 'targetAttribute' => ['idam' => 'id']],
            [['idjugadores'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::class, 'targetAttribute' => ['idjugadores' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idjugadores' => 'Idjugadores',
            'idam' => 'Idam',
        ];
    }

    /**
     * Gets query for [[Idam0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdam0()
    {
        return $this->hasOne(Animalesmarinos::class, ['id' => 'idam']);
    }

    /**
     * Gets query for [[Idjugadores0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdjugadores0()
    {
        return $this->hasOne(Jugadores::class, ['id' => 'idjugadores']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jugadores".
 *
 * @property int $id
 * @property string|null $firstName
 * @property string|null $lastName
 * @property string $username
 * @property string $pasword
 * @property string|null $auth_key
 *
 * @property Capturan[] $capturans
 * @property Compran[] $comprans
 * @property Consiguen[] $consiguens
 * @property Desentierran[] $desentierrans
 * @property Animalesmarinos[] $idams
 * @property Bichos[] $idbichos
 * @property Fosiles[] $idfosils
 * @property Obrasdearte[] $idobras
 * @property Peces[] $idpeces
 * @property Pescan[] $pescans
 */
class Jugadores extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jugadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'pasword'], 'required'],
            [['firstName', 'lastName', 'username', 'pasword'], 'string', 'max' => 30],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['pasword'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstName' => 'First Name',
            'lastName' => 'Last Name',
            'username' => 'Username',
            'pasword' => 'Pasword',
            'auth_key' => 'Auth Key',
        ];
    }

    /**
     * Gets query for [[Capturans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCapturans()
    {
        return $this->hasMany(Capturan::class, ['idjugadores' => 'id']);
    }

    /**
     * Gets query for [[Comprans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComprans()
    {
        return $this->hasMany(Compran::class, ['idjugadores' => 'id']);
    }

    /**
     * Gets query for [[Consiguens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConsiguens()
    {
        return $this->hasMany(Consiguen::class, ['idjugadores' => 'id']);
    }

    /**
     * Gets query for [[Desentierrans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDesentierrans()
    {
        return $this->hasMany(Desentierran::class, ['idjugadores' => 'id']);
    }

    /**
     * Gets query for [[Idams]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdams()
    {
        return $this->hasMany(Animalesmarinos::class, ['id' => 'idam'])->viaTable('consiguen', ['idjugadores' => 'id']);
    }

    /**
     * Gets query for [[Idbichos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdbichos()
    {
        return $this->hasMany(Bichos::class, ['id' => 'idbichos'])->viaTable('capturan', ['idjugadores' => 'id']);
    }

    /**
     * Gets query for [[Idfosils]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdfosils()
    {
        return $this->hasMany(Fosiles::class, ['id' => 'idfosil'])->viaTable('desentierran', ['idjugadores' => 'id']);
    }

    /**
     * Gets query for [[Idobras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdobras()
    {
        return $this->hasMany(Obrasdearte::class, ['id' => 'idobra'])->viaTable('compran', ['idjugadores' => 'id']);
    }

    /**
     * Gets query for [[Idpeces]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdpeces()
    {
        return $this->hasMany(Peces::class, ['id' => 'idpeces'])->viaTable('pescan', ['idjugadores' => 'id']);
    }

    /**
     * Gets query for [[Pescans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPescans()
    {
        return $this->hasMany(Pescan::class, ['idjugadores' => 'id']);
    }

    public function getAuthKey() {
        return $this->authKey;
    }

    public function getId() {
        return $this->id;
    }

    public function validateAuthKey($authKey) {
        $this->authKey === $authKey;
    }

    public static function findIdentity($id) {
        return self::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException();
    }
    
    public static function findByUsername($username){
        return self::findOne(['username'=>$username]);
    }

    public function validatePassword($password){
        return $this->password === $password;
    }
}

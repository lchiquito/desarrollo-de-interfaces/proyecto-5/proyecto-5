<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mesespeces".
 *
 * @property int $id
 * @property int|null $idpeces
 * @property string|null $meses
 *
 * @property Peces $idpeces0
 */
class Mesespeces extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mesespeces';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idpeces'], 'integer'],
            [['meses'], 'string', 'max' => 10],
            [['idpeces', 'meses'], 'unique', 'targetAttribute' => ['idpeces', 'meses']],
            [['idpeces'], 'exist', 'skipOnError' => true, 'targetClass' => Peces::class, 'targetAttribute' => ['idpeces' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idpeces' => 'Idpeces',
            'meses' => 'Meses',
        ];
    }

    /**
     * Gets query for [[Idpeces0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdpeces0()
    {
        return $this->hasOne(Peces::class, ['id' => 'idpeces']);
    }
}

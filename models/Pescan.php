<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pescan".
 *
 * @property int $id
 * @property int|null $idjugadores
 * @property int|null $idpeces
 *
 * @property Jugadores $idjugadores0
 * @property Peces $idpeces0
 */
class Pescan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pescan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idjugadores'], 'integer'],
            [['idjugadores'], 'match', 'pattern' => "/^[0-9]+$/", 'message' => 'Sólo se aceptan números.'],
            [['idpeces'], 'integer'],
            [['idpeces'], 'match', 'pattern' => "/^[0-9]+$/", 'message' => 'Sólo se aceptan números.'],
            [['idjugadores', 'idpeces'], 'unique', 'targetAttribute' => ['idjugadores', 'idpeces']],
            [['idjugadores'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::class, 'targetAttribute' => ['idjugadores' => 'id']],
            [['idpeces'], 'exist', 'skipOnError' => true, 'targetClass' => Peces::class, 'targetAttribute' => ['idpeces' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idjugadores' => 'Idjugadores',
            'idpeces' => 'Idpeces',
        ];
    }

    /**
     * Gets query for [[Idjugadores0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdjugadores0()
    {
        return $this->hasOne(Jugadores::class, ['id' => 'idjugadores']);
    }

    /**
     * Gets query for [[Idpeces0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdpeces0()
    {
        return $this->hasOne(Peces::class, ['id' => 'idpeces']);
    }
}

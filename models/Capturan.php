<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "capturan".
 *
 * @property int $id
 * @property int|null $idjugadores
 * @property int|null $idbichos
 *
 * @property Bichos $idbichos0
 * @property Jugadores $idjugadores0
 */
class Capturan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'capturan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idjugadores', 'idbichos'], 'integer', 'min' => '1'],
            [['idjugadores', 'idbichos'], 'unique', 'targetAttribute' => ['idjugadores', 'idbichos']],
            [['idbichos'], 'exist', 'skipOnError' => true, 'targetClass' => Bichos::class, 'targetAttribute' => ['idbichos' => 'id']],
            [['idjugadores'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::class, 'targetAttribute' => ['idjugadores' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idjugadores' => 'Idjugadores',
            'idbichos' => 'Idbichos',
        ];
    }

    /**
     * Gets query for [[Idbichos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdbichos0()
    {
        return $this->hasOne(Bichos::class, ['id' => 'idbichos']);
    }

    /**
     * Gets query for [[Idjugadores0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdjugadores0()
    {
        return $this->hasOne(Jugadores::class, ['id' => 'idjugadores']);
    }
}

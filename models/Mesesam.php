<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mesesam".
 *
 * @property int $id
 * @property int|null $idam
 * @property string|null $meses
 *
 * @property Animalesmarinos $idam0
 */
class Mesesam extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mesesam';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idam'], 'integer'],
            [['idam'], 'match', 'pattern' => "/^[0-9]+$/", 'message' => 'Sólo se aceptan números.'],
            [['meses'], 'string', 'max' => 10],
            [['meses'], 'match', 'pattern' =>  "/^[a-zA-ZáéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÙÄËÏÖÜñÑ-.,;\s]+$/" ,'message' => 'No se pueden introducir números.'],
            [['idam', 'meses'], 'unique', 'targetAttribute' => ['idam', 'meses']],
            [['idam'], 'exist', 'skipOnError' => true, 'targetClass' => Animalesmarinos::class, 'targetAttribute' => ['idam' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idam' => 'Idam',
            'meses' => 'Meses',
        ];
    }

    /**
     * Gets query for [[Idam0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdam0()
    {
        return $this->hasOne(Animalesmarinos::class, ['id' => 'idam']);
    }
}

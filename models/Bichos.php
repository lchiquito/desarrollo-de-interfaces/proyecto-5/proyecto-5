<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bichos".
 *
 * @property int $id
 * @property string|null $nombre
 * @property int|null $precio
 * @property string|null $tiempo
 * @property string|null $lugaraparicion
 *
 * @property Capturan[] $capturans
 * @property Jugadores[] $idjugadores
 * @property Mesesbichos[] $mesesbichos
 */
class Bichos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bichos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['precio'], 'integer'],
            [['nombre'], 'string', 'max' => 45],
            [['tiempo', 'lugaraparicion'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'precio' => 'Precio',
            'tiempo' => 'Tiempo',
            'lugaraparicion' => 'Lugar de aparición',
        ];
    }

    /**
     * Gets query for [[Capturans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCapturans()
    {
        return $this->hasMany(Capturan::class, ['idbichos' => 'id']);
    }

    /**
     * Gets query for [[Idjugadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdjugadores()
    {
        return $this->hasMany(Jugadores::class, ['id' => 'idjugadores'])->viaTable('capturan', ['idbichos' => 'id']);
    }

    /**
     * Gets query for [[Mesesbichos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMesesbichos()
    {
        return $this->hasMany(Mesesbichos::class, ['idbichos' => 'id']);
    }
}

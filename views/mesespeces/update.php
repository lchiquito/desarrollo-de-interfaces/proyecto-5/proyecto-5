<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Mesespeces $model */

$this->title = 'Update Mesespeces: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Mesespeces', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mesespeces-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

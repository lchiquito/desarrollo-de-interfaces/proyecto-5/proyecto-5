<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Mesespeces $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="mesespeces-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idpeces')->textInput() ?>

    <?= $form->field($model, 'meses')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

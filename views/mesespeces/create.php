<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Mesespeces $model */

$this->title = 'Create Mesespeces';
$this->params['breadcrumbs'][] = ['label' => 'Mesespeces', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mesespeces-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\bootstrap4\Html;

$this->title = 'WikiNook';
?>
<!-- CARRUSEL DE IMÁGENES DE LA HOME -->
<body>
    
    <br>
    <h1> WikiNook </h1>
    
    
    <h5>En esta guía completa del juego podrás aprovechar al máximo tu partida de Animal Crossing y completar la parte más divertida del juego.<br>
        Dirigido a principiantes y veteranos.</h5>
    <br>
    <br>
   
    
    
    <!--Carrusel della pantalla de inicio con sus respectivas imágenes -->
    
    <div id="carouselAnimalCrossing" class="carousel slide" data-ride="carousel">   
        <ol class="carousel-indicators">
            <li data-target="#carouselAnimalCrossing" data-slide-to="0" class="active"></li>
            <li data-target="#carouselAnimalCrossing" data-slide-to="1" class="active"></li>
            <li data-target="#carouselAnimalCrossing" data-slide-to="2" class="active"></li>
            <li data-target="#carouselAnimalCrossing" data-slide-to="3" class="active"></li>
            <li data-target="#carouselAnimalCrossing" data-slide-to="4" class="active"></li>
        </ol>

        <div class="row justify-content-center">        
            <div class=" col-md-12">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <?= Html::img('@web/img/animalcrossing1.jpg', ['class' => 'd-block w-100'], ['alt' => 'My logo']) ?>
                        <div class="carousel-caption d-none d-md-block" style="color:#4d2a08">
                            <h3>Animal Crossing: Happy Home Paradise</h3>
                            <p>¡Con este DLC podrás sacar tus dotes de diseñador de interiores y crear las mejores casa para tus aldeanos favoritos!</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <?= Html::img('@web/img/animalcrossing2.jpg', ['class' => 'd-block w-100'], ['alt' => 'My logo']) ?>
                         <div class="carousel-caption d-none d-md-block" style="color:#4d2a08">
                             <h3>Pesca y submarinismo</h3>
                             <p>Con una caña y un traje de buceo puedes conseguir cualquier animal marino que imagines, ¡patos al agua!</p>
                         </div>
                    </div>
                    <div class="carousel-item">
                        <?= Html::img('@web/img/animalcrossing3.jpg', ['class' => 'd-block w-100'], ['alt' => 'My logo']) ?>
                        <div class="carousel-caption d-none d-md-block" style="color:#4d2a08">
                            <h3>Caza de insectos</h3>
                            <p>Desde una mosca hasta las mariposas más bonitas o los escorpiones más agresivos, ¡coleccionalos a todos o dónalos al museo!</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <?= Html::img('@web/img/museoanimalcrossing.jpg', ['class' => 'd-block w-100'], ['alt' => 'My logo']) ?>
                        <div class="carousel-caption d-none d-md-block" style="color:#fffdd6">
                            <h3>El museo de Sócrates</h3>
                            <p>Dona al museo cualquier captura que hagas, ¡Sócrates lo agradecerá! Además tendrás un museo muy completo que podrás visitar.</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <?= Html::img('@web/img/ladinoanimalcrossing.jpg', ['class' => 'd-block w-100'], ['alt' => 'My logo']) ?>
                        <div class="carousel-caption d-none d-md-block" style="color:#fffdd6">
                            <h3>Ladino el timador</h3>
                            <p>Este astuto zorro que visita tu isla en un viejo barco intentará sacarte hasta la última de tus bayas ¡cuidado con lo que le compras! Aunque a veces vende cuadros que podrían interesarte...</p>
                        </div>
                    </div>
                <a href="#carouselAnimalCrossing" class="carousel-control-prev" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a href="#carouselAnimalCrossing" class="carousel-control-next" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>

</body>



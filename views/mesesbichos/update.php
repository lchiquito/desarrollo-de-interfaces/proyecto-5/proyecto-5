<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Mesesbichos $model */

$this->title = 'Update Mesesbichos: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Mesesbichos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mesesbichos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

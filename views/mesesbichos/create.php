<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Mesesbichos $model */

$this->title = 'Create Mesesbichos';
$this->params['breadcrumbs'][] = ['label' => 'Mesesbichos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mesesbichos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

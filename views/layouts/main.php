<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
    <title><?= Html::encode($this->title) ?></title>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100 fontAtma", style="background-color: #fffdd6;">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar my-navbar navbar-expand-md navbar-dark fixed-top ',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'items' => [
            
            ['label' => 'Obras de arte', 'url' => ['/obrasdearte/consulta-obras-arte']],
            ['label' => 'Fósiles', 'url' => ['/fosiles/consulta-fosiles']],
            ['label' => 'Peces', 'url' => ['/peces/consulta-peces']],
            ['label' => 'Criaturas submarinas', 'url' => ['/animalesmarinos/consulta-animalesmarinos']],
            ['label' => 'Bichos', 'url' => ['/bichos/consulta-bichos']],
           /* 
            ['label' => 'Jugadores', 'url' => ['/jugadores/index']],
            ['label' => 'Capturan', 'url' => ['/capturan/index']],
            ['label' => 'Compran', 'url' => ['/compran/index']],
            ['label' => 'Consiguen', 'url' => ['/consiguen/index']],
            ['label' => 'Desentierran', 'url' => ['/desentierran/index']],
            ['label' => 'Mesesam', 'url' => ['/mesesam/index']],
            ['label' => 'Mesespeces', 'url' => ['/mesespeces/index']],
            ['label' => 'Pescan', 'url' => ['/pescan/index']],
            ['label' => 'Contact', 'url' => ['/site/contact']],
          
           */
           
          
            
        ],
    ]);
            
            
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>
<footer class="footer mt-auto py-3 text-muted">
    <div class="container" style="color:white">
        <p class="float-left">&copy; WikiNook <?= date('Y') ?></p>
        <p class="float-right">&copy; Desarrollado por un super fan </p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Animalesmarinos $model */

$this->title = 'Update Animalesmarinos: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Animalesmarinos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="animalesmarinos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

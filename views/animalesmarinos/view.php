<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Animalesmarinos $model */

$this->title = $model->nombre;
\yii\web\YiiAsset::register($this);
?>
<div class="animalesmarinos-view">
    <br>
    <h1><?= Html::encode($this->title) ?></h1>
    <br>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'precio',
            'sombra',
            'velocidad',
        ],
    ]) ?>

</div>

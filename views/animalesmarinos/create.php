<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Animalesmarinos $model */

$this->title = 'Create Animalesmarinos';
$this->params['breadcrumbs'][] = ['label' => 'Animalesmarinos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="animalesmarinos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

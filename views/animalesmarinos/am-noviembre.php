<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use yii\widgets\ListView;
/* titulo de la parte de la página en la que estamos*/
$this->title = 'Criaturas submarinas';
?>
<? desplegable de los tipos de sombra de los animales marinos ?>

<div class="btn-group btn-desplegablesombracs " role="group">
     <button id="btnGroupDrop1" type="button" class="btn custom-button dropdown-toggle custom-button btn-sombra" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Sombra
     </button>
        <div class="dropdown-menu btn-sombra" aria-labelledby="btnGroupDrop1">
            <p>
              <?= Html::a('Muy pequeña', ['animalesmarinos/muypequena'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Pequeña', ['animalesmarinos/pequena'], ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Mediana', ['animalesmarinos/mediana'], ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Grande', ['animalesmarinos/grande'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
            </p>
        </div>
</div>

<? desplegable de los tipos de velocidad de los animales marinos ?>

<div class="btn-group btn-desplegablevelocidad" role="group">
     <button id="btnGroupDrop1" type="button" class="btn custom-button dropdown-toggle custom-button btn-lugar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Velocidad
     </button>
        <div class="dropdown-menu btn-lugar" aria-labelledby="btnGroupDrop1">
            <p>
              <?= Html::a('Sin velocidad', ['animalesmarinos/sinvelocidad'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Muy lenta', ['animalesmarinos/muylenta'], ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Lenta', ['animalesmarinos/lenta'], ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Mediana', ['animalesmarinos/normal'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Rápida', ['animalesmarinos/rapida'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Muy rápida', ['animalesmarinos/muyrapida'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
            </p>
        </div>
</div>

<? desplegable de los meses que sale cada animal marino ?>

<div class="btn-group btn-desplegablemesesam " role="group">
     <button id="btnGroupDrop1" type="button" class="btn custom-button dropdown-toggle custom-button btn-lugar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Meses
     </button>
        <div class="dropdown-menu btn-lugar" aria-labelledby="btnGroupDrop1">
            <p>
              <?= Html::a('Enero', ['animalesmarinos/amenero'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Febrero', ['animalesmarinos/amfebrero'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Marzo', ['animalesmarinos/ammarzo'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Abril', ['animalesmarinos/amabril'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Mayo', ['animalesmarinos/ammayo'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Junio', ['animalesmarinos/amjunio'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Julio', ['animalesmarinos/amjulio'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Agosto', ['animalesmarinos/amagosto'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Septiembre', ['animalesmarinos/amseptiembre'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Octubre', ['animalesmarinos/amoctubre'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Noviembre', ['animalesmarinos/amnoviembre'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Diciembre', ['animalesmarinos/amdiciembre'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
            </p>
        </div>
</div>

<div class="row">
    <div class=" body-content">
        <h1 class="encabezadoTitulo">Criaturas submarinas</h1>
        <h5> Para conseguirlos necesitas ponerte un bañador y bucear por el mar.</h5>
            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_consulta-animales-marinos',
                'layout' => "{items}",
                'itemOptions' => [
                    'class' => 'list-view-tarjetas',
                    'style' => 'float: left; justify-content: center; margin-top: 10px; width:30%;'
                ],
            ]);
            ?>
        
    </div>
</div>

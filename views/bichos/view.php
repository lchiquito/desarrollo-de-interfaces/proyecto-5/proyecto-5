<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Bichos $model */

$this->title = $model->nombre;
\yii\web\YiiAsset::register($this);
?>
<div class="bichos-view">
    <br>
    <h1><?= Html::encode($this->title) ?></h1>
    <br>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'precio',
            'tiempo',
            'lugaraparicion',
        ],
    ]) ?>

</div>

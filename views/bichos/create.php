<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Bichos $model */

$this->title = 'Create Bichos';
$this->params['breadcrumbs'][] = ['label' => 'Bichos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bichos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use app\models\Bichos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Bichos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bichos-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            

            'id',
            'nombre',
            'precio',
            'tiempo',
            'lugaraparicion',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Bichos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>

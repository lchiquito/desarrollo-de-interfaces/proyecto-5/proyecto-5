<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Bichos $model */

$this->title = 'Update Bichos: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Bichos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bichos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

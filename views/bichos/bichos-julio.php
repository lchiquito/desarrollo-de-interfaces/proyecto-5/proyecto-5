<?php
use yii\widgets\ListView;
use yii\helpers\html;
?>

<? desplegable del tiempo atmosférico en el que aparecer los bichos ?>
<div class="btn-group btn-desplegabletiempo" role="group">
     <button id="btnGroupDrop1" type="button" class="btn custom-button dropdown-toggle custom-button btn-lugar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Tiempo
     </button>
        <div class="dropdown-menu btn-lugar" aria-labelledby="btnGroupDrop1">
            <p>
              <?= Html::a('Siempre', ['bichos/siempre'],['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Siempre menos con lluvia', ['bichos/siempresinlluvia'], ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Solo con lluvia', ['bichos/soloconlluvia'], ['class'=>'btn custom-button btn-dentrodesple'])?>
            </p>
        </div>
</div>

<? desplegable del lugar de aparición de los bichos ?>
<div class="btn-group btn-desplegableaparicion" role="group">
     <button id="btnGroupDrop1" type="button" class="btn custom-button dropdown-toggle custom-button btn-lugar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Lugar de aparición
     </button>
        <div class="dropdown-menu btn-lugar" aria-labelledby="btnGroupDrop1">
            <p>
              <?= Html::a('Cerca de flores', ['bichos/cercadeflores'],['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Agitando árboles', ['bichos/agitandoarboles'], ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('En la orilla del mar', ['bichos/orillademar'], ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Rocas o arbustos', ['bichos/rocasarbustos'],['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('En las flores', ['bichos/flores'], ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Charcas o ríos', ['bichos/charcas'], ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Golpeando rocas', ['bichos/rocas'],['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('En los árboles', ['bichos/enarboles'], ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('En palmeras', ['bichos/palmeras'], ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('En el suelo', ['bichos/suelo'],['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('En tocones', ['bichos/tocones'], ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Volando', ['bichos/volando'], ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('En aldeanos', ['bichos/aldeanos'], ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('En la hierba', ['bichos/hierba'], ['class'=>'btn custom-button btn-dentrodesple'])?>
            </p>
        </div>
</div>

<? desplegable de los meses que sale cada bicho ?>

<div class="btn-group btn-desplegablemesesbichos " role="group">
     <button id="btnGroupDrop1" type="button" class="btn custom-button dropdown-toggle custom-button btn-lugar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Meses
     </button>
        <div class="dropdown-menu btn-lugar" aria-labelledby="btnGroupDrop1">
            <p>
              <?= Html::a('Enero', ['bichos/bichosenero'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Febrero', ['bichos/bichosfebrero'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Marzo', ['bichos/bichosmarzo'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Abril', ['bichos/bichosabril'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Mayo', ['bichos/bichosmayo'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Junio', ['bichos/bichosjunio'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Julio', ['bichos/bichosjulio'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Agosto', ['bichos/bichosagosto'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Septiembre', ['bichos/bichosseptiembre'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Octubre', ['bichos/bichosoctubre'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Noviembre', ['bichos/bichosnoviembre'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Diciembre', ['bichos/bichosdiciembre'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
            </p>
        </div>
</div>


<div class="row">
    <div class=" body-content">
        <h1 class="encabezadoObras">Bichos</h1>
        <h5>Si no tienes cuidado igual te llevas una picadura de alguno de estos bichos, ¡ándate con ojo!</h5>
<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_consulta-bichos', // Nombre de la vista parcial para cada ítem
    'layout' => "{items}\n{pager}", // Diseño de la lista
        'itemOptions' => [
                    'style' => 'float: left; justify-content: center; margin-top: 10px; width:33%; ',
                ],
]) ?>
<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

?>
<div class="row">
<div class="col-sm-12" >
    <div class="card alturaminima">
        <div class="card-body tarjeta imagenAc">

            <h3 class="tituloAc"><?= Html::img('@web/img/bichos/' . $model->nombre . '.png', ['alt' => 'My logo']) ?></h3>
            <h3> <?= $model->nombre ?></h3>
                <p>
                     <?= Html::a('Ver datos', ['bichos/view', 'id' => $model->id], ['class' => 'btn custom-button'])?>
                </p>
        </div>
    </div>
</div>
</div>
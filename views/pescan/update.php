<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Pescan $model */

$this->title = 'Update Pescan: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pescans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pescan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Pescan $model */

$this->title = 'Create Pescan';
$this->params['breadcrumbs'][] = ['label' => 'Pescans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pescan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Capturan $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="capturan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idjugadores')->textInput() ?>

    <?= $form->field($model, 'idbichos')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Capturan $model */

$this->title = 'Update Capturan: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Capturans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="capturan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Desentierran $model */

$this->title = 'Update Desentierran: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Desentierrans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="desentierran-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

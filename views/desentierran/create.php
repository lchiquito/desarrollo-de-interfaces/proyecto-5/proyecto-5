<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Desentierran $model */

$this->title = 'Create Desentierran';
$this->params['breadcrumbs'][] = ['label' => 'Desentierrans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="desentierran-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

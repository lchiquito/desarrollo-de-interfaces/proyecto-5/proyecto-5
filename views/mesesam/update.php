<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Mesesam $model */

$this->title = 'Update Mesesam: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Mesesams', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mesesam-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

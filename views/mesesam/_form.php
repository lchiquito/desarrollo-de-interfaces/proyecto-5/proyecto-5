<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Mesesam $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="mesesam-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idam')->textInput() ?>

    <?= $form->field($model, 'meses')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Obrasdearte $model */

$this->title = 'Create Obrasdearte';
$this->params['breadcrumbs'][] = ['label' => 'Obrasdeartes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="obrasdearte-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


/** @var yii\web\View $this */
/** @var app\models\Obrasdearte $model */

$this->title = $model->titulo;
\yii\web\YiiAsset::register($this);
?>
<div class="obrasdearte-view">
    <br>
    <h1><?= Html::encode($this->title) ?></h1>
    <br>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            
            'titulo',
            'autor',
            'falsificacion',
            'explicacion',
        ],
    ]) ?>

</div>

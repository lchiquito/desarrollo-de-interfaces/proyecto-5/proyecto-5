<?php
use yii\widgets\ListView;
use yii\helpers\html;
?>

<? desplegable de falsificaciones ?>

<div class="btn-group btn-desplegableobras" role="group">
     <button id="btnGroupDrop1" type="button" class="btn custom-button dropdown-toggle custom-button btn-lugar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Falsificaciones
     </button>
        <div class="dropdown-menu btn-lugar" aria-labelledby="btnGroupDrop1">
            <p>
              <?= Html::a('Con falsificación', ['obrasdearte/confalsificacion'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Sin falsificación', ['obrasdearte/sinfalsificacion'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
            </p>
        </div>
</div>


<div class="row">
    <div class=" body-content">
        <h1 class="encabezadoObras">Obras de arte</h1>
        <h5>Si compras una obra falsa no podrás exponerla en el museo así que ¡comprueba bien lo que compras!</h5>
<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_consulta-obras-arte', // Nombre de la vista parcial para cada ítem
    'layout' => "{items}\n{pager}", // Diseño de la lista
        'itemOptions' => [
                    'style' => 'float: left; justify-content: center; margin-top: 10px; width:33%; ',
                ],
]) ?>
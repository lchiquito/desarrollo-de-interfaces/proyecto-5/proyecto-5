<?php

use app\models\Obrasdearte;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\widgets\ListView;
use yii\grid\GridView;


/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Obras de arte';

//** codigo para cambiar de color el botón de "Conseguido" *//
$js = <<< JS
var button = $('#my-button');

button.on('click', function() {
    button.css('background-color', '#4aab3e'); // Cambia el color del fondo del botón a rojo
    button.css('border-color', '#4aab3e');
});
JS;

$this->registerJs($js);
?>




<?php
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'titulo',
        'autor',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}',
            'buttons' => [
                'view' => function ($url, $model, $key) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model->id], [
                        'title' => Yii::t('yii', 'View'),
                        'class' => 'btn btn-primary btn-xs',
                    ]);
                },
            ],
        ],
    ],
]);
?>

<div class="obrasdearte-index">
    <div class="tarjeta tituloObras">

    <h1><?= Html::encode($this->title) ?></h1>
    <h4>Las 41 obras de arte de Ladino </h4>
    
<!-- ESTE BOTÓN NO ES NECESARIO
    <p>
        <?= Html::a('Create Obras de arte', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
 -->    
<!--INICIO DE LA MAQUETACIÓN-->
<div class="body-content">
        <div class="body">

    <!--INICIO DE LA PRIMERA FILA-->

        <div class="row">
                <!--INICIO DE TARJETA**********************************************************-->
            <div class="card-spacing mb-3">
                <div class="card" style="width: 18rem;">

                    <img src="<?= Yii::getAlias('@web')?>/img/cuadroanatomico.webp" class="card-img-top" alt="cuadro anatomico">
                    <div class="card-body">
                        
                        <h3 class="card-title">Cuadro anatómico</h3>
                        
                           <?= Html::button('Conseguido', ['id' => 'my-button', 'class' => 'btn btn-danger']) ?>
                            
                           <a class="btn btn-primary" role="button">Ver</a>
                        </p>
                    </div>
                </div>
                <!--FIN DE TARJETA **************************************************-->
                <!--INICIO DE TARJETA *********************************************************-->
                 <div class="card" style="width: 18rem;">

                    <img src="<?= Yii::getAlias('@web')?>/img/cuadroholandes.webp" class="card-img-top" alt="cuadro holandes">
                    <div class="card-body">

                        <h3 class="card-title">Cuadro holandés</h3>
                       
                            <a class="btn btn-primary" href="http://historia.lavuelta.com/es/corredor.asp?id=SUI19610327_1" role="button">Vencedor</a>
                        </p>
                    </div>
                </div>
                <!--FIN DE TARJETA ************************************************-->
                <!--INICIO DE TARJETA ******************************************************* -->
                 <div class="card" style="width: 18rem;">

                    <img src="<?= Yii::getAlias('@web')?>/img/estatuaprimitiva.webp" class="card-img-top" alt="estatua primitiva">
                    <div class="card-body">

                        <h3 class="card-title">Estatua primitiva</h3>
   
                            <a class="btn btn-primary" href="http://historia.lavuelta.com/es/corredor.asp?id=SUI19610327_1" role="button">Vencedor</a>
                        </p>
                    </div>
                </div>
                <!--FIN DE TARJETA *************************************************-->
                <!--INICIO DE TARJETA ******************************************************* -->
                 <div class="card" style="width: 18rem;">

                    <img src="<?= Yii::getAlias('@web')?>/img/retratoheroico.webp" class="card-img-top" alt="retrato heroico">
                    <div class="card-body">

                        <h3 class="card-title">Retrato heroico</h3>
                     
                            <a class="btn btn-primary" href="http://historia.lavuelta.com/es/corredor.asp?id=SUI19610327_1" role="button">Vencedor</a>
                        </p>
                    </div>
                </div>
                <!--FIN DE TARJETA *************************************************-->
                
            </div>
        </div> <!-- FIN DE LA PRIMERA FILA -->
    
        <!-- INICIO DE LA SEGUNDA FILA -->
        <div class="row">
                <!--boton de consulta-->
                <div class="card-spacing mb-3">
                <div class="card" style="width: 18rem;">

                    <img src="<?= Yii::getAlias('@web')?>/img/cuadroalegorico.webp" class="card-img-top" alt="cabeza colosal">
                    <div class="card-body">

                        <h3 class="card-title">Cuadro anatómico</h3>
                       
                            <a class="btn btn-primary" href="http://historia.lavuelta.com/es/corredor.asp?id=SUI19610327_1" role="button">Vencedor</a>
                        </p>
                    </div>
                </div>
                </div>
        </div>
        </div>
</div> 
                
    <!--final de la maquetación-->
          
    </div>

</div>

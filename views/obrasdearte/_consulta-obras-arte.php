<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
?>

<!-- ItemView de las tarjetas de Obras de arte -->

<div class="row">
<div class="col-sm-12" >
    <div class="card alturaminima">
        <div class="card-body tarjeta imagenAc ">

            <h3 class="tituloObra"><?= Html::img('@web/img/obras/' . $model->titulo . '.png', ['alt' => 'My logo']) ?></h3>
            <h3> <?= $model->titulo ?></h3>
            <div class="button-container">
               
                
                <?= Html::a('Ver datos', ['obrasdearte/view', 'id' => $model->id], ['class' => 'btn custom-button'])?>

              
            </div>
            
        </div>
    </div>
</div>
</div>












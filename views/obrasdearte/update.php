<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Obrasdearte $model */

$this->title = 'Update Obrasdearte: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Obrasdeartes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="obrasdearte-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

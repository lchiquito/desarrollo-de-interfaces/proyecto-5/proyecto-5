<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use yii\widgets\ListView;
/* titulo de la parte de la página en la que estamos*/
$this->title = 'Fósiles';
?>
<? desplegable de las partes de los fósiles ?>
<div class="btn-group btn-desplegablepartes " role="group">
     <button id="btnGroupDrop1" type="button" class="btn custom-button dropdown-toggle custom-button btn-sombra" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Partes
     </button>
        <div class="dropdown-menu btn-sombra" aria-labelledby="btnGroupDrop1">
            <p>
              <?= Html::a('Una parte', ['fosiles/1'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Dos partes', ['fosiles/2'], ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Tres partes', ['fosiles/3'], ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Cuarto partes', ['fosiles/4'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
            </p>
        </div>
</div>

<div class="row">
    <div class=" body-content">
        <h1 class="encabezadoTitulo">Fósiles</h1>
        <h5>Quedan súper chulos en el museo pero si necesitas dinero no dudes en venderlos por una buena suma</h5>
           <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_consulta-fosiles',
                'layout' => "{items}",
                'itemOptions' => [
                    'class' => 'list-view-tarjetas',
                    'style' => 'float: left; justify-content: center; margin-top: 10px; width:32%;'
                ],
            ]);
            ?>
        
    </div>
</div>

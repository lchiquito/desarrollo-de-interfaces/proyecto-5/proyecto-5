<?php
use yii\widgets\ListView;
use yii\helpers\html;
?>

<? desplegable de los tipos de sombra de los peces ?>

<div class="btn-group btn-desplegablepartes2 " role="group">
     <button id="btnGroupDrop1" type="button" class="btn custom-button dropdown-toggle custom-button btn-sombra" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Partes
     </button>
        <div class="dropdown-menu btn-sombra" aria-labelledby="btnGroupDrop1">
            <p>
              <?= Html::a('Una parte', ['fosiles/1'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Dos partes', ['fosiles/2'], ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Tres partes', ['fosiles/3'], ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Cuarto partes', ['fosiles/4'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
            </p>
        </div>
</div>



<div ">
    <div class=" body-content">
        <h1 class="encabezadoObras">Fósiles</h1>
         <h5>Quedan súper chulos en el museo pero si necesitas dinero no dudes en venderlos por una buena suma</h5>
<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_consulta-fosiles', // Nombre de la vista parcial para cada ítem
    'layout' => "{items}\n{pager}", // Diseño de la lista
        'itemOptions' => [
                    'style' => 'display: inline-block; width: -10%; ',
                ],
]) ?>

    </div>
</div>  
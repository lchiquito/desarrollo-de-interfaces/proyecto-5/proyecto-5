<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Fosiles $model */

$this->title = $model->nombre;
\yii\web\YiiAsset::register($this);
?>
<div class="fosiles-view">
    <br>
    <h1><?= Html::encode($this->title) ?></h1>
    <br>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'partes',
            'precio',
        ],
    ]) ?>

</div>

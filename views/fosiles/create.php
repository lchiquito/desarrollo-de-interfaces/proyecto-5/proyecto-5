<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Fosiles $model */

$this->title = 'Create Fosiles';
$this->params['breadcrumbs'][] = ['label' => 'Fosiles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fosiles-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

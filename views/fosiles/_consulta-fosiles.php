<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

?>

<div class="card alturaminima">
    <div class="card-body tarjeta imagenAc">

        <h3 class="tituloFosiles"><?= Html::img('@web/img/fosiles/' . $model->nombre . '.png', ['alt' => 'My logo']) ?></h3>
        <h3> <?= $model->nombre ?></h3>
            <p>
                <?= Html::a('Ver datos', ['fosiles/view', 'id' => $model->id], ['class' => 'btn custom-button'])?>

            </p>
    </div>
</div>

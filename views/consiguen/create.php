<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Consiguen $model */

$this->title = 'Create Consiguen';
$this->params['breadcrumbs'][] = ['label' => 'Consiguens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consiguen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

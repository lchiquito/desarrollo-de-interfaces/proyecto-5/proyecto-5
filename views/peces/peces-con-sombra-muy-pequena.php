<?php
use yii\widgets\ListView;
use yii\helpers\html;
?>

<? desplegable de los tipos de sombra de los peces ?>

<div class="btn-group btn-desplegablesombra " role="group">
     <button id="btnGroupDrop1" type="button" class="btn custom-button dropdown-toggle custom-button btn-sombra" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Sombra
     </button>
        <div class="dropdown-menu btn-sombra" aria-labelledby="btnGroupDrop1">
            <p>
              <?= Html::a('Muy pequeña', ['peces/muypequena'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Pequeña', ['peces/pequena'], ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Mediana', ['peces/mediana'], ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Grande', ['peces/grande'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Gigante', ['peces/gigante'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Estrecha', ['peces/estrecha'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
            </p>
        </div>
</div>

<? desplegable de los lugares de aparición de los peces ?>

<div class="btn-group btn-desplegablelugar " role="group">
     <button id="btnGroupDrop1" type="button" class="btn custom-button dropdown-toggle custom-button btn-lugar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Aparición
     </button>
        <div class="dropdown-menu btn-lugar" aria-labelledby="btnGroupDrop1">
            <p>
              <?= Html::a('Rio', ['peces/rio'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Estanque', ['peces/estanque'], ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Mar', ['peces/mar'], ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Muelle', ['peces/muelle'], ['class'=>'btn custom-button btn-dentrodesple'])?>
            </p>
        </div>
</div>

<? desplegable de los meses que sale cada pez ?>

<div class="btn-group btn-desplegablemeses " role="group">
     <button id="btnGroupDrop1" type="button" class="btn custom-button dropdown-toggle custom-button btn-lugar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      Meses
     </button>
        <div class="dropdown-menu btn-lugar" aria-labelledby="btnGroupDrop1">
            <p>
              <?= Html::a('Enero', ['peces/pecesenero'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Febrero', ['peces/pecesfebrero'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Marzo', ['peces/pecesmarzo'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Abril', ['peces/pecesabril'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Mayo', ['peces/pecesmayo'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Junio', ['peces/pecesjunio'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Julio', ['peces/pecesjulio'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Agosto', ['peces/pecesagosto'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Septiembre', ['peces/pecesseptiembre'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Octubre', ['peces/pecesoctubre'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Noviembre', ['peces/pecesnoviembre'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
              <?= Html::a('Diciembre', ['peces/pecesdiciembre'],   ['class'=>'btn custom-button btn-dentrodesple'])?>
            </p>
        </div>
</div>



<div class="row">
    <div class=" body-content">
        <h1 class="encabezadoObras">Peces</h1>
        <h5>Algunos ejemplares son más complicados de capturar que otros así que ármate de paciencia.</h5>
<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_consulta-peces', // Nombre de la vista parcial para cada ítem
    'layout' => "{items}\n{pager}", // Diseño de la lista
        'itemOptions' => [
                    'style' => 'float: left; justify-content: center; margin-top: 10px; width:33%; ',
                ],
]) ?>
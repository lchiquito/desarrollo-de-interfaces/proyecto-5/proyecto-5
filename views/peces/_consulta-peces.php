<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

?>
<div class="row">
<div class="col-sm-12" >
    <div class="card alturaminima">
        <div class="card-body tarjeta imagenAc">

            <h3 class="tituloObra"><?= Html::img('@web/img/peces/' . $model->nombre . '.png', ['alt' => 'My logo']) ?></h3>
            <h3> <?= $model->nombre ?></h3>
                <p>
                         <?= Html::a('Ver datos', ['peces/view', 'id' => $model->id], ['class' => 'btn custom-button'])?>
                </p>
        </div>
    </div>
</div>
</div>


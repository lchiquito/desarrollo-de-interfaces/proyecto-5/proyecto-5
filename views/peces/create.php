<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Peces $model */

$this->title = 'Create Peces';
$this->params['breadcrumbs'][] = ['label' => 'Peces', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="peces-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use app\models\Peces;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Peces';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="peces-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Peces', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
         

            'id',
            'nombre',
            'precio',
            'sombra',
            'lugaraparicion',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Peces $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
